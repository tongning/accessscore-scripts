This folder contains example HTML/JS code for creating various web-based visualizations using Mapbox GL JS and output from
street-viz-tools.

You can see the visualizations by entering the desired subfolder and running:

```bash
python3 -m http.server
```
and visiting `http://localhost:8000` in your browser.