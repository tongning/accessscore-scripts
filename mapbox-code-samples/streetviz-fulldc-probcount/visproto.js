function drawStreetLayerProblemCount() {
    d3.json("http://localhost:8000/data/v2-result-fulldc.geojson", function (err, data) {
        map.addSource("streets-with-scores", {
            "type": "geojson",
            "data": data
        });

        
        map.addLayer({
            'id': 'streets-by-avg-severity',
            'type': 'line',
            'source': 'streets-with-scores',
            'paint': {
                'line-color': [
                    "interpolate",
                    ["linear"],
                    ["get", "scoreByCount"],
                    0,
                    "#ffeee0",
                    180,
                    "#ff8b0f",
                    250,
                    "hsl(0, 100%, 44%)",
                    1159.342174255452,
                    "#e00000"
                  ],
                'line-width': 3,
                'line-opacity': 0.9,
            }
        });
    });
}

function drawNeighborhoods() {

    // North Cleveland Park - 205
    // Hillsdale - 231

    console.log("drawNeighborhoods");
    d3.json("http://localhost:8000/data/neighborhoods.json", function (err, data) {
        map.addSource("neighborhoods", {
            "type": "geojson",
            "data": data
        });

        // Border layer
        map.addLayer({
            'id': 'neighborhood-border',
            'type': 'line',
            'source': 'neighborhoods',
            'paint': {
                'line-color': '#ffffff', // Dark: '#ffffff' //Light: '#606C38', 
                'line-width': 1,
                'line-opacity': 0.9,
                'line-dasharray': [2, 4]
            }
        });
    });
}
