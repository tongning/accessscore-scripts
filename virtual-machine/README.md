### Geo Tools VM
If you are having trouble running any of the tools in this repository, you can try running them in this preconfigured Ubuntu 18.10 VM.

[Link to download the VM](https://drive.google.com/file/d/1d7PQ5v1UWmAuEKtEoU9YZIqIL-DKzz23/view?usp=sharing)

#### Login info
The VM is configured to log in automatically, but should you need sudo permission to install additional software, the password to the default account is `ubuntu`.

#### How to use
This accessscore-tools repository has already been cloned to the home folder, so you should be able to simply open a terminal, `cd accessscore-tools`, and follow each tool's respective instructions to run them. Any required python/npm packages should have already been installed, though if you have updated the repository via `git pull`, you may wish to rerun package installation steps in case any new dependencies have been added.

#### Python virtual environment
When you open a terminal window, a Python 3 virtual environment containing required packages will be activated automatically. If you do not like this behavior, simply remove the last line from `~/.bashrc`.
