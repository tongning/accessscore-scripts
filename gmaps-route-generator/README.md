# gmaps-route-generator

Given an origin point and a geojson file of accessibility labels, this tool will compute walking routes to various points of interest near the origin point and find accessibility labels near the routes. The result is saved as a JSON file.

### How to use

First, save your geojson file of accessibility labels to `input/labels.geojson`. An example file is already present.

Next, create a file named `apikey` in the root of this folder and paste your Google Maps API key.

Finally, install dependencies and run the tool:

```bash
$ pip install -r requirements.txt
$ python main.py
```

The result is saved in result.json.