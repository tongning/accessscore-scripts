import googlemaps
import polyline
import geopandas
import json
from geojson import Point, Feature, FeatureCollection, dump
from shapely.geometry import Point, Polygon, MultiPolygon, LineString

from lib.destination import Destination
from lib.route import Route

def find_destinations_by_search_queries(query_lat, query_lon, search_queries=["grocery", "restaurant", "school", "coffee", "park", "museum", "hospital"], num_to_find=1):
    destination_list = []
    for query in search_queries:
        response = client.places(
            query, 
            location=(query_lat, query_lon),
            radius = 1609)
        if len(response['results']) == 0:
            continue
            
        # For each query, retrieve up to num_to_find_results
        for i in range(0, num_to_find):
            if len(response['results']) > i:
                poi_location = response['results'][i]['geometry']['location']
                
                destination_list.append(Destination(poi_location['lat'], poi_location['lng'], query))
            else:
                break
    return destination_list

def find_destinations_by_nearby_places(query_lat, query_lon):
    destination_list = []
    result = client.places_nearby(location=(query_lat, query_lon), radius=3000)
    
    places_list = result['results']
    print(f"Found {len(places_list)} nearby places")
    for place in places_list:
        lat = place['geometry']['location']['lat']
        lon = place['geometry']['location']['lng']
        poi_name = place['name'] if 'name' in place else "Unknown POI"
        destination_list.append(Destination(lat, lon, poi_name))
    
    return destination_list

def find_route_to_destination(query_lat, query_lon, destination):
    api_result = client.directions(
            (query_lat, query_lon),
            (destination.lat, destination.lon), 
            mode="walking")
    
    return Route(api_result, destination)

# https://geopandas.readthedocs.io/en/latest/gallery/create_geopandas_from_pandas.html
def read_labels_to_geopandas_dataframe(filename):
    with open(filename,'r') as data_file:
        data = json.load(data_file)
    feature_collection = FeatureCollection(data['features'])
    return geopandas.GeoDataFrame.from_features(feature_collection['features'])

def get_spatial_index_from_geopandas_dataframe(dataframe):
    return dataframe.sindex 

def get_polygon_around_route(route):
    route_coords = route.getCoordListLonLat()
    linestring = LineString(route_coords)
    polygon = linestring.buffer(0.0004)

    return polygon

def get_labels_within_polygon(labels, labels_index, polygon):
    possible_matches_index = list(labels_index.intersection(polygon.bounds))
    possible_matches = labels.iloc[possible_matches_index]
    precise_matches = possible_matches[possible_matches.intersects(polygon)]
    points_within_geometry = precise_matches
    return points_within_geometry

def compute_delay_for_single_label(label):
    delay_secs = 0
    if label['label_type'] == "NoCurbRamp":
        delay_secs = 4
    elif label['label_type'] == "SurfaceProblem":
        delay_secs = 1
    elif label['label_type'] == "Obstacle":
        delay_secs = 2
    
    if label['severity'] == 1:
        delay_secs = 0
    elif label['severity'] == 3:
        delay_secs *= 1.5
    elif label['severity'] == 4:
        delay_secs *= 3
    elif label['severity'] == 5:
        delay_secs *= 6
    
    return delay_secs

def compute_delay_for_labels(labels):
    labels['delay'] = labels.apply(compute_delay_for_single_label, axis=1)
    return labels

def filter_problem_labels(labels_dataframe):
    return labels_dataframe.query('label_type == "NoCurbRamp" | label_type == "Obstacle" | label_type == "SurfaceProblem"')

def generate_poi_trip(destination, route, problem_labels_in_polygon):
    dataframe_without_geometry = problem_labels_in_polygon.drop(['geometry'], axis=1)
    delay_seconds = problem_labels_in_polygon['delay'].sum()
    poi_trip = {
        'POI': {'coords': [destination.lon, destination.lat],
                'poi_type': destination.destination_type},
        'duration_seconds': route.getDurationSeconds(),
        'delay_seconds': delay_seconds,
        'duration_with_delay': route.getDurationSeconds() + delay_seconds,
        'distance_meters': route.getDistanceMeters(),
        'route': {'geojson': route.asGeoJson(), 'labels_nearby': dataframe_without_geometry.to_dict('records')}
    }
    return poi_trip

def read_api_key(filename):
    try:
        with open(filename, 'r') as f:
            # It's assumed our file contains a single line,
            # with our API key
            return f.read().strip()
    except FileNotFoundError:
        print("'%s' file not found" % filename)

client = googlemaps.Client(key=read_api_key("apikey"))
'''
# Sample location
myLat = 38.898
myLon = -77.039

# Georgetown
myLat = 38.906833
myLon = -77.06608

# N Cleveland Park
myLat = 38.951444
myLon = -77.071500

# Downtown
myLat = 38.903750
myLon = -77.036556

# Brightwood
myLat = 38.962694
myLon = -77.027806
'''
# SW waterfront
myLat = 38.877139
myLon = -77.017722

limit_pois_to_search_queries = False

labels = read_labels_to_geopandas_dataframe("input/labels.geojson")
labels_index = get_spatial_index_from_geopandas_dataframe(labels)

result = {
    'origin': {
        'coords': [myLon, myLat],
        'POI_Trips': []
    },
}

destinations = None

if limit_pois_to_search_queries:
    destinations = find_destinations_by_search_queries(myLat,myLon)
else:
    destinations = find_destinations_by_nearby_places(myLat, myLon)

for destination in destinations:
    route = find_route_to_destination(myLat,myLon, destination)
    polygon = get_polygon_around_route(route)

    labels_in_polygon = get_labels_within_polygon(labels, labels_index, polygon)
    labels_in_polygon = compute_delay_for_labels(labels_in_polygon)
    problem_labels_in_polygon = filter_problem_labels(labels_in_polygon)

    poi_trip_object = generate_poi_trip(destination, route, problem_labels_in_polygon)
    result['origin']['POI_Trips'].append(poi_trip_object)

with open('result.json', 'w') as outfile:
    json.dump(result, outfile)
    print("Wrote result.json")
