import polyline
import json
from shapely_geojson import dumps, Feature
from shapely.geometry import Point, Polygon, MultiPolygon, LineString

class Route:
    def __init__(self, api_result, destination):
        self.api_result = api_result
        self.destination = destination
    
    def getCoordList(self):
        return polyline.decode(self.api_result[0]['overview_polyline']['points'])

    def getCoordListLonLat(self):
        result = []
        coord_list_lat_lon = self.getCoordList()
        for coord in coord_list_lat_lon:
            result.append((coord[1], coord[0]))
        return result

    def getDurationSeconds(self):
        return self.api_result[0]['legs'][0]['duration']['value']

    def getDistanceMeters(self):
        return self.api_result[0]['legs'][0]['distance']['value']

    def asGeoJson(self):
        return json.loads(dumps(LineString(self.getCoordListLonLat()), indent=2))



