class Destination:
    def __init__(self, lat, lon, destination_type):
        self.lat = lat
        self.lon = lon 
        self.destination_type = destination_type
    def __str__(self):
        return f'Destination(lat={self.lat},lon={self.lon},destination_type={self.destination_type})'