
var turf = require('@turf/turf');
var fs = require('fs');

// GEOJSON file of accessibility labels. Each label should have at least a 'severity' and 'label_type' property
var labels = fs.readFileSync('./input/labels_filtered.geojson');

// GEOJSON file of the street network
var streets = fs.readFileSync('./input/full_dc.geojson');

/**********CONFIGURATION OPTIONS *************/
// Whether to include bad curbramps (those with severity >= 4) as accessibility-harming features in score calculations
var includeBadCurbRamps = true;

// Enable score smoothing for generating violin plots. The streets input file MUST come from linechunker.js output to use this option.
var violinMode = false;

// If enabled, all computations will be done using only labels in a small neighborhood of DC to speed up debugging. Should be disabled for normal use.
var smallNeighborhoodDebugMode = false;


/*******END OF CONFIGURATION OPTIONS ***********/

/* Code begins below */

labelPoints = JSON.parse(labels);
streetSegments = JSON.parse(streets);

/* Checks whether a label is one of (NoCurbRamp/SurfaceProblem/Obstacle) 
based on the label_type string */
function isNegativePointLabel(labeltype) {
    if (labeltype === 'NoCurbRamp' ||
    labeltype === 'SurfaceProblem' ||
    labeltype === 'Obstacle') {
        return true;
    }
    return false;
}

/* Checks whether a label is one of 
(NoCurbRamp/SurfaceProblem/Obstacle/CurbRamp with severity >= 4) 
based on the label_type string and severity */
function isNegativePointLabelOrBadCurbramp(labeltype, severity) {
    if (isNegativePointLabel(labeltype)) {
        return true;
    }
    if (labeltype === 'CurbRamp' && severity >=4) {
        return true;
    }
    return false;
}

/* Compute the average severity of labels in a list of labels */
function computeSegmentScoreBySeverity(pointsNearSegment) {
    let totalSeverity = 0;
    let totalPoints = 0;
    for(let i = 0; i < pointsNearSegment.length; i++) {
        let pt = pointsNearSegment[i];
        /* Skip various labels types that should not be included */
        if (includeBadCurbRamps && 
            !isNegativePointLabelOrBadCurbramp(pt['properties']['label_type'],pt['properties']['severity'])){
            continue;
        }
        if (!includeBadCurbRamps && 
            !isNegativePointLabel(pt['properties']['label_type'])) {
            continue;
        }
        let ptSeverity = pt['properties']['severity'];
        if (isNaN(ptSeverity)) {
            continue;
        }
        if(!(ptSeverity >=1 && ptSeverity <= 5)) {
            continue;
        }
        totalSeverity += ptSeverity;
        totalPoints += 1;
    }
    return totalSeverity / totalPoints;
}

/* A very simple scoring model that includes both label count and severity. */
function computeSegmentScoreByCombinedModel(pointsNearSegment, segmentLength) {
    let score=null;
    if (pointsNearSegment.length > 5) {
        score = computeSegmentScoreBySeverity(pointsNearSegment)/5;
    } else {
        score = computeSegmentScoreByCount(pointsNearSegment, segmentLength)/300.0
    }
    return score;
}

/* A scoring model based on feature count, computed as problem count per kilometer */
function computeSegmentScoreByCount(pointsNearSegment, segmentLength) {
    let featurecounts = 
    {
        curbRampCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'CurbRamp';
        }).length,
        badCurbRampCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'CurbRamp' &&
            value['properties']['severity'] >= 4;
        }).length,
        noCurbRampCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'NoCurbRamp';
        }).length,
        surfaceProblemCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'SurfaceProblem';
        }).length,
        obstacleCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'Obstacle';
        }).length,
        noSidewalkCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'NoSidewalk';
        }).length,
        otherCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'Other';
        }).length,
        occlusionCount: pointsNearSegment.filter((value, index, array) => {
            return value['properties']['label_type'] === 'Occlusion';
        }).length
    };

    // Count number of accessibility-harming features
    let badFeatureCount = 
        featurecounts['noCurbRampCount'] + 
        featurecounts['surfaceProblemCount'] +
        featurecounts['obstacleCount'];
    if (includeBadCurbRamps) {
        badFeatureCount += featurecounts['badCurbRampCount'];
    }

    // Divide bad feature count by segment length in km
    let score = badFeatureCount / segmentLength;
    return score;
}


function avg (v) {
    return v.reduce((a,b) => a+b, 0)/v.length;
  }
  
function smoothOut (vector, variance) {
    var t_avg = avg(vector)*variance;
    var ret = Array(vector.length);
    for (var i = 0; i < vector.length; i++) {
      (function () {
        var prev = i>0 ? ret[i-1] : vector[i];
        var next = i<vector.length ? vector[i] : vector[i-1];
        ret[i] = avg([t_avg, avg([prev, vector[i], next])]);
      })();
    }
    return ret;
}


let streetSegmentList = streetSegments['features'];
let pointsListRaw = labelPoints;
let boundingBox = turf.polygon([
    [
      [
        -77.008,
        38.899
      ],
      [
        -77.008,
        38.920
      ],
      [
        -76.971,
        38.920
      ],
      [
        -76.971,
        38.899
      ],

      [
        -77.008,
        38.899
      ]
    ]
  ]);

var pointsList = null;
if (smallNeighborhoodDebugMode) {
    pointsList = turf.pointsWithinPolygon(pointsListRaw, boundingBox)['features'];
} else {
    pointsList = pointsListRaw['features'];
}

// Labels that are NoCurbRamp, Obstacle, SUrfaceProblem
pointsListNegativePointLabel = pointsList.filter((value, index, array) => {
    return isNegativePointLabel(value['properties']['label_type']);
});

// Labels that are NoCurbRamp, Obstacle, SUrfaceProblem, or CurbRamp w/ severity >= 4
pointsListNegativePointLabelOrBadCr = pointsList.filter((value, index, array) => {
    return isNegativePointLabelOrBadCurbramp(value['properties']['label_type'],value['properties']['severity']);
});

pointsFeatureCollection = turf.featureCollection(pointsListNegativePointLabel)
// Iterate through the street segments and compute various scores for each segment
for(let i=0; i < streetSegmentList.length; i++) {
    let currSegment = streetSegmentList[i];
    if (currSegment['geometry']['type']!=='LineString'){
        continue;
    }
    
    // Find points near this street segment
    let buffered = turf.buffer(currSegment, 30, {units: 'meters'})
    let pointsInsideBuffer = turf.pointsWithinPolygon(pointsFeatureCollection, buffered)['features'];

    // Compute score for this segment based on points found
    let segmentLength = turf.length(currSegment, {units: 'kilometers'});
    let segmentScore = computeSegmentScoreByCount(pointsInsideBuffer, segmentLength);
    let segmentScoreCombined = computeSegmentScoreByCombinedModel(pointsInsideBuffer, segmentLength);
    let segmentScoreSeverity = computeSegmentScoreBySeverity(pointsInsideBuffer);
 
    // Save the scores
    currSegment['properties']['scoreByCount'] = segmentScore;
    currSegment['properties']['scoreBySeverity'] = segmentScoreSeverity;
    currSegment['properties']['scoreByCombined'] = segmentScoreCombined;

    // Progress indicator
    console.log("ScoreCalc:"+(i+1)+"/"+streetSegmentList.length);
}

// When in violin mode 'smooth out' the scores along each street segment
if (violinMode) {
    let startPtr = 0;
    let endPtr = 0;

    while(endPtr < streetSegments['features'].length) {
        if (streetSegments['features'][endPtr]['properties']['endofstreet']) {
            let segmentScores = [];
            for(let i = startPtr; i<= endPtr; i++) {
                segmentScores.push(streetSegments['features'][i]['properties']['scoreByCount']);
            }
            let smooth = smoothOut(segmentScores, 0.85);
            let curr = 0;
            for(let i = startPtr; i<= endPtr; i++) {
                streetSegments['features'][i]['properties']['scoreByCount'] = smooth[curr];
                curr++;
            }
            endPtr++;
            startPtr = endPtr;
        } else {
            endPtr++;
        }
        if (endPtr % 100 === 0) {
            console.log("Violin plot:"+(endPtr+1)+"/"+streetSegments['features'].length);
        }
    }
}

fs.writeFileSync('./result.geojson', JSON.stringify(streetSegments));
/* Write debug files containing labels included in computation */
fs.writeFileSync('./labels_negativepoint.geojson', JSON.stringify(turf.featureCollection(pointsListNegativePointLabel)));
fs.writeFileSync('./labels_negativepointwithbadcr.geojson', JSON.stringify(turf.featureCollection(pointsListNegativePointLabelOrBadCr)));

console.log('Saved result.geojson');
console.log('Saved debug file labels_negativepoint.geojson');
console.log('Saved debug file labels_negativepointwithbadcr.geojson');
