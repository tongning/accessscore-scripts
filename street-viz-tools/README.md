# street-accessibility-viz-tools

This tool generates accessibility scores for street segments, which can be visualized using Mapbox Studio. It is written in Javascript (node.js) using the [turf.js](http://turfjs.org/) Javascript geospatial analysis framework.


## Basic usage

Inputs and outputs are in GEOJSON format. There are two required input files:
* A geojson file containing the street network
* A geojson file containing accessibility labels. At a minimum, each label should have a `label_type`and `severity` property.

To run, set the paths to the input files at the top of `index.js` and adjust any of the configuration options as needed. With `node` and `npm` installed on your machine, run:

```bash
$ npm install
$ node index.js
```
The output will be saved to `result.geojson`. The result file will contain the street network with various score properties added to each street segment. It can be uploaded as a tileset to Mapbox Studio for visualization.

### Configuration

A few configuration options are available at the top of the `index.js` script. See the comments for descriptions.

### Generating violin plots

A violin plot visualization can provide more granular information about where on a street segment problems are located. To generate a violin plot visualization, the street network must be partitioned using the `linechunker.js` script. Use the following procedure:
1. Update `linechunker.js` with the path to the street network
2. Run 
```bash
$ node linechunker.js
```
3. Use the output file as the input street network to `index.js`, as above. Be sure to enable the violin mode option in `index.js`.
