
/*
This script processes a street network and splits each street segment into smaller segments
each 10 meters in length. The segment that marks the end of each original segment is marked with the 'endofstreet'
property.
*/

var turf = require('@turf/turf');
var fs = require('fs');

var streets = JSON.parse(fs.readFileSync('./input/streets-original.geojson'))['features'];
var chunkedstreets = [];
for (let i=0; i<streets.length; i++) {
  
    let chunked = turf.lineChunk(streets[i], 10, {units: 'meters'})['features'];
    chunked[chunked.length-1]['properties']['endofstreet'] = true;
   
    chunkedstreets = chunkedstreets.concat(chunked);
}

var result = turf.featureCollection(chunkedstreets);
fs.writeFileSync('./city-chunked.geojson', JSON.stringify(result));
console.log("Wrote city-chunked.geojson");
